#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
struct ColorCode(u8);
impl ColorCode {
    fn new(fg: Color, bg: Color) -> Self {
        Self((bg as u8) << 4 | (fg as u8))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct ScreenChar {
    ascii_char: u8,
    color_code: ColorCode,
}

// NOTE: completely arbitrary, change later
const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;

#[repr(transparent)]
struct Buffer {
    chars: [[ScreenChar; BUFFER_WIDTH]; BUFFER_HEIGHT],
}
impl Buffer {
    /// Returns a mutable reference to the VGA buffer.
    pub fn vga_buffer() -> &'static mut Self {
        unsafe { &mut *(0xb8000 as *mut Buffer) }
    }

    /// Writes a byte at the given position in the buffer, and assigns the given `ColorCode` to it.
    pub fn write_byte_at(&mut self, byte: u8, color_code: ColorCode, pos: (usize, usize)) {
        self.chars[pos.0][pos.1] = ScreenChar {
            ascii_char: byte,
            color_code,
        };
    }

    /// Writes a string starting from the given position in the buffer, and assigns the given
    /// `ColorCode` to every `char` written.
    pub fn write_str_at(&mut self, s: &str, color_code: ColorCode, mut pos: (usize, usize)) {
        for byte in s.bytes() {
            match byte {
                b'\n' => {
                    pos.0 += 1;
                    pos.1 = 0;
                }
                0x20..=0x7e => self.write_byte_at(byte, color_code, pos),
                // not part of the printable ASCII range, print '■'
                _ => self.write_byte_at(0xfe, color_code, pos),
            }

            pos.1 += 1;
        }
    }
}

pub fn writing_test() {
    let mut buf = Buffer::vga_buffer();

    buf.write_byte_at(b'H', ColorCode::new(Color::Yellow, Color::Black), (0, 0));
    buf.write_str_at("ello ", ColorCode::new(Color::Red, Color::Magenta), (0, 1));
    buf.write_str_at("Wörld!", ColorCode::new(Color::Red, Color::Magenta), (1, 0));
}
